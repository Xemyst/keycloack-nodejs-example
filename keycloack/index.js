var session = require('express-session');
var Keycloak = require('keycloak-connect');
const {
  keycloack: {
    clientId,
    url,
    realm,
    secret,
  }
} = require('../config')
let _keycloak;

const keycloakConfig = {
    clientId: clientId,
    bearerOnly: true,
    serverUrl: url,
    realm: realm,
    credentials: {
        secret: secret
    }
};

function initKeycloak() {
    if (_keycloak) {
        console.warn("Trying to init Keycloak again!");
        return _keycloak;
    }
    else {
        console.log("Initializing Keycloak...");
        var memoryStore = new session.MemoryStore();
        _keycloak = new Keycloak({ store: memoryStore }, keycloakConfig);
        return _keycloak;
    }
}

function getKeycloak() {
    if (!_keycloak){
        console.error('Keycloak has not been initialized. Please called init first.');
    }
    return _keycloak;
}

module.exports = {
    initKeycloak,
    getKeycloak
};
