KEYCLOACK_URL=http://localhost:8080
KEYCLOACK_REALM=test-realm
KEYCLOACK_CLIENT_ID=test-nodejs-example
KEYCLOACK_SECRET=jpSxcj5WXD7KswS9C6VTxwg3nPjShdTm
USER=eric
PASSWORD=test 

curl -X POST "${KEYCLOACK_URL}/realms/${KEYCLOACK_REALM}/protocol/openid-connect/token" \
 --header 'Content-Type: application/x-www-form-urlencoded' \
 --data-urlencode 'grant_type=password' \
 --data-urlencode "client_id=${KEYCLOACK_CLIENT_ID}" \
 --data-urlencode "username=${USER}" \
 --data-urlencode "password=${PASSWORD}" \
 --data-urlencode "client_secret=${KEYCLOACK_SECRET}"
