const express = require('express')
const app = express()
const { port } = require('./config')

let keycloack = require('./keycloack/index.js')
keycloack = keycloack.initKeycloak()

app.use(keycloack.middleware());

const testController = require('./routes/test');
app.use('/test', testController);

app.get('/', function(req, res){
   res.send("Server is up!");
});


app.listen(port)
