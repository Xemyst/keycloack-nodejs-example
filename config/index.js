require('dotenv').config();

const config = {
  keycloack: {
    secret: process.env.KEYCLOACK_SECRET,
    clientId: process.env.KEYCLOACK_CLIENT_ID,
    url: process.env.KEYCLOACK_URL,
    realm: process.env.KEYCLOACK_REALM
  },
  port: process.env.NODE_PORT || 8080
};


module.exports = config;
