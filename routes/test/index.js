const express = require('express');
const router = express.Router();

const keycloack = require('../../keycloack').getKeycloak();
console.log(keycloack)
router.get('/anonymous', function(req, res){
    res.send("Hello Anonymous");
});

router.get('/user', keycloack.protect('user'), function(req, res){
    res.send("Hello User");
});

router.get('/admin', keycloack.protect('admin'), function(req, res){
    res.send("Hello Admin");
});

router.get('/all-user', keycloack.protect(['user','admin']), function(req, res){
    res.send("Hello All User");
});

module.exports = router;
