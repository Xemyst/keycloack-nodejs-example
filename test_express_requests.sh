SERVICE_URL=http://localhost:8081
ACCESS_TOKEN=$1

echo ""

curl -X GET "${SERVICE_URL}/test/anonymous" \
--header "Authorization: bearer ${ACCESS_TOKEN}"

echo ""

curl -X GET "${SERVICE_URL}/test/user" \
--header "Authorization: bearer ${ACCESS_TOKEN}"

echo ""

curl -X GET "${SERVICE_URL}/test/admin" \
--header "Authorization: bearer ${ACCESS_TOKEN}"

echo ''

curl -X GET "${SERVICE_URL}/test/all-user" \
--header "Authorization: bearer ${ACCESS_TOKEN}"
