NODE_ENV=development

KEYCLOACK_ADMIN_USER=admin
KEYCLOACK_ADMIN_PASSWORD=admin
KEYCLOACK_PORT=8080

configure:
	npm i

docker-build:
	docker build -t ${REPO_FULL_PATH}:${VERSION} .

docker-push:
	make docker-build
	docker push ${REPO_FULL_PATH}:${VERSION}

test:
	npm run test

run-keycloack:
	docker run -p ${KEYCLOACK_PORT}:8080 -e KEYCLOAK_ADMIN=${KEYCLOACK_ADMIN_USER} -e KEYCLOAK_ADMIN_PASSWORD=${KEYCLOACK_ADMIN_PASSWORD} quay.io/keycloak/keycloak:18.0.0 start-dev

dev:
	npm run dev

